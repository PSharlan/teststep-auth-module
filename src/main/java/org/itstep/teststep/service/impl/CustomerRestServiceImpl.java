package org.itstep.teststep.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.itstep.teststep.model.Customer;
import org.itstep.teststep.service.CustomerRestService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Slf4j
@Service
public class CustomerRestServiceImpl implements CustomerRestService {

    //FIXME
    private final RestTemplate restTemplate = new RestTemplate();
    private static final String CUSTOMER_SERVICE_URL = "http://localhost:8071/api/v1/customers/username/";

    @Override
    public Optional<Customer> findCustomerByUsername(String username) {
        log.info("findCustomerByUsername() -> username: " + username);
        final Customer customer = restTemplate
                .getForObject(CUSTOMER_SERVICE_URL + username, Customer.class);
        log.info("findCustomerByUsername() -> found customer: " + customer);
        return Optional.ofNullable(customer);
    }
}
