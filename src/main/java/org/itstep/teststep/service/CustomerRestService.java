package org.itstep.teststep.service;

import org.itstep.teststep.model.Customer;

import java.util.Optional;

public interface CustomerRestService {

    Optional<Customer> findCustomerByUsername(String username);
}
