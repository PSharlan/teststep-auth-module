package org.itstep.teststep.security;

import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.itstep.teststep.model.Customer;
import org.itstep.teststep.model.Role;
import org.itstep.teststep.service.CustomerRestService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {

    //FIXME
    @Value("${jwt.secret-key}")
    private String secret;
    private final static long TIME_OUT = 600000; // 10 min
    private static final String PREFIX = "Bearer_";

    private final CustomerRestService customerService;

    @PostConstruct
    protected void init() {
        secret = Base64.getEncoder().encodeToString(secret.getBytes());
    }

    public String createToken(String username) {
        Customer customer = customerService.findCustomerByUsername(username)
                .orElseThrow(() -> new JwtAuthenticationException("User not found"));
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", getRoleNames(customer.getRoles()));
        claims.put("name", customer.getUsername());

        Date now = new Date();
        Date validity = new Date(new Date().getTime() + TIME_OUT);

        return PREFIX + Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    Authentication getAuthentication(String token) {
        Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
        JwtPrincipal principal = new JwtPrincipal(claims.getBody().get("name", String.class),
                                                  claims.getBody().get("roles", List.class));

        return new JwtAuthenticationToken(principal);
    }

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith(PREFIX)) {
            return bearerToken.substring(PREFIX.length());
        }
        return null;
    }

    boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException("JWT token is expired or invalid");
        }
    }

    private List<String> getRoleNames(List<Role> roles) {
        return roles.stream().map(Role::getName).collect(Collectors.toList());
    }

    static class JwtAuthenticationException extends AuthenticationException {
        JwtAuthenticationException(String msg) {
            super(msg);
        }
    }

}
