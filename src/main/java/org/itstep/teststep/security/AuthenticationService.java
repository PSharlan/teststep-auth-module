package org.itstep.teststep.security;

import lombok.AllArgsConstructor;
import org.itstep.teststep.model.AuthenticationRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@AllArgsConstructor
public class AuthenticationService {

    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    public Map<Object, Object> getToken(AuthenticationRequest req) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPassword()));
        String token = jwtTokenProvider.createToken(req.getUsername());
        Map<Object, Object> responseMap = new HashMap<>();
        responseMap.put("token", token);
        return responseMap;
    }
}
