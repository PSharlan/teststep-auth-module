package org.itstep.teststep.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.security.auth.Subject;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class JwtPrincipal implements Principal {

    private String username;
    private Collection<? extends GrantedAuthority> authorities;

    JwtPrincipal(String username, Collection authorities){
        this.username = username;
        this.authorities = toAuthorities(authorities);
    }

    private Collection<? extends GrantedAuthority> toAuthorities(Collection authorities) {
        Collection<String> roles = new ArrayList<>();
        authorities.forEach(auth -> roles.add((String)auth));
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }

    Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
