package org.itstep.teststep.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.itstep.teststep.model.Customer;
import org.itstep.teststep.service.CustomerRestService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private CustomerRestService customerRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Customer customer = customerRepository.findCustomerByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        return JwtUserDetailsFactory.create(customer);
    }
}
