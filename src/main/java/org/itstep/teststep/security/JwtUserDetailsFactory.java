package org.itstep.teststep.security;

import org.itstep.teststep.model.Customer;
import org.itstep.teststep.model.Role;
import org.itstep.teststep.model.Status;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public class JwtUserDetailsFactory {

    public static JwtUserDetails create(Customer customer){
        return new JwtUserDetails(customer.getUsername(),
                customer.getPassword(),
                customer.getStatus().equals(Status.ACTIVE),
                mapToGrantedAuthorities(customer.getRoles()));
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }
}
