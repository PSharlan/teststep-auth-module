package org.itstep.teststep.api;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.itstep.teststep.model.AuthenticationRequest;
import org.itstep.teststep.security.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")
@Api(value = "/api/v1/auth", description = "Manage customers login")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody AuthenticationRequest req) {
        return ResponseEntity.ok(authenticationService.getToken(req));
    }
}
