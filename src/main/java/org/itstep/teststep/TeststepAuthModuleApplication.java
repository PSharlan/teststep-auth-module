package org.itstep.teststep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeststepAuthModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeststepAuthModuleApplication.class, args);
	}

}
