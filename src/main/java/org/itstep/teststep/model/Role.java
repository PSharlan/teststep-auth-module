package org.itstep.teststep.model;

import lombok.Data;

@Data
public class Role {

    private Long id;
    private String name;

}
