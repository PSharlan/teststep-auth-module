package org.itstep.teststep.model;

import lombok.Data;

import java.util.List;

@Data
public class Customer {

        private Long id;
        private String username;
        private String email;
        private String password;
        private List<Role> roles;
        private Status status;
}
